#!/bin/sh

set -e # if anything fails, return an error

envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf

nginx -g 'daemon off;' # tell nginx to run on the foreground so outputs and logs are printed in the Docker output